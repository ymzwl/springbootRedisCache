package com.ymzwl.test;

import java.util.Date;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * TestController
 *
 * @author ZENG.XIAO.YAN
 * @version 1.0
 * @Date 2019-08-02
 */
@Service
public class TestDao {
	//一个叫 name传入参数的 缓存 会存储到redis里
    @Cacheable(cacheNames = "testDaoCache",key = "#name")
    public String helloUser(String name) {
    	System.out.println("你好："+name+"，没有执行缓存");
        return "{\"jsonkey\",\"jsonvalue\"}";// 模拟从持久层获取数据
    }
    public String helloUserNoCache(String name) {
    	System.out.println("你好："+name+"，没有执行缓存");
        return "{\"jsonkey\",\"jsonvalue\"}";// 模拟从持久层获取数据
    }
}
