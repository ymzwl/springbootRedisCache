package com.ymzwl.test;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TestController
 *
 * @author ZENG.XIAO.YAN
 * @version 1.0
 * @Date 2019-08-02
 */
@RestController
public class TestController extends BaseController {
	@Autowired
	private TestDao testDao;

	@GetMapping("/helloUser")
	public String helloUser(HttpServletRequest request, HttpServletResponse response) {
		String name = request.getParameter("name");
		outJson(response, testDao.helloUser(name));
		return null;
	}

	@GetMapping("/helloUserNoCache")
	public String helloUserNoCache(HttpServletRequest request, HttpServletResponse response) {
		String name = request.getParameter("name");
		outJson(response, testDao.helloUserNoCache(name));
		return null;
	}

	@PostConstruct
	public void init() {
	}
}
