package com.ymzwl.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class Mycat {
	// MySQL 8.0 以下版本 - JDBC 驱动名及数据库 URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
    //static final String DB_URL = "jdbc:mysql://localhost:8066/mycat_db";
    //static final String DB_URL = "jdbc:mysql://192.168.0.253:3306/mycat_t";
    static final String DB_URL = "jdbc:mysql://localhost:3307/mycat_t1";
 
    // MySQL 8.0 以上版本 - JDBC 驱动名及数据库 URL
    //static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";  
    //static final String DB_URL = "jdbc:mysql://localhost:3306/RUNOOB?useSSL=false&serverTimezone=UTC";
 
 
    // 数据库的用户名与密码，需要根据自己的设置
    static final String USER = "root";
    static final String PASS = "root";
    static Connection conn = null;
    static Statement stmt = null;
	public static void main(String[] args) {
		try {
			 // 注册 JDBC 驱动
	        Class.forName(JDBC_DRIVER);
	        
	        // 打开链接
            System.out.println("连接数据库...");
            if(conn==null) {
            	conn = DriverManager.getConnection(DB_URL,USER,PASS);
            	//conn.setAutoCommit(false);
            }
            
        
            
	        
//			for (int i = 0; i < 100000; i++) {
//				String sql="INSERT INTO user (id, name, age) VALUES  ('"+Util.uuid()+"', '"+Util.uuid()+"', "+i+")";
//				executeSql(sql);
//				System.out.println("第"+i+"条");
//			}
	        String sql="";
	        
	        conn.setAutoCommit(false);
	        stmt = conn.createStatement();
	        //sql="START TRANSACTION";
			//executeSql(sql);
	        sql="UPDATE money SET num=num-5 WHERE NAME='z3'";
	        stmt.addBatch(sql);
	        stmt.executeBatch();
	        //conn.commit();
			//executeSql(sql);
			//conn.rollback();
			//conn.setAutoCommit(true);
			//sql="UPDATE money SET num=num+5 WHERE NAME='l4'";
			//executeSql(sql);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void executeSql(String sql) {
		
        
        try{
           
        	// 执行查询
            System.out.println(" 实例化Statement对象...");
            stmt = conn.createStatement();
          
            
            stmt.executeUpdate(sql);
        

            // 完成后关闭
            //stmt.close();
            //conn.close();
        }catch(SQLException se){
            // 处理 JDBC 错误
            se.printStackTrace();
        }catch(Exception e){
            // 处理 Class.forName 错误
            e.printStackTrace();
        }
        System.out.println("语句执行完成!");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	Connection conn = null;
    Statement stmt = null;
    try{
        // 注册 JDBC 驱动
        Class.forName(JDBC_DRIVER);
    
        // 打开链接
        System.out.println("连接数据库...");
        conn = DriverManager.getConnection(DB_URL,USER,PASS);
    
        // 执行查询
        System.out.println(" 实例化Statement对象...");
        stmt = conn.createStatement();
        String sql;
        sql = "SELECT * FROM user";
        ResultSet rs = stmt.executeQuery(sql);
    
        // 展开结果集数据库
        while(rs.next()){
            // 通过字段检索
            String id  = rs.getString("id");
            String name = rs.getString("name");

            // 输出数据
            System.out.print("ID: " + id);
            System.out.print(", 名称: " + name);
            System.out.print("\n");
        }
        // 完成后关闭
        rs.close();
        stmt.close();
        conn.close();
    }catch(SQLException se){
        // 处理 JDBC 错误
        se.printStackTrace();
    }catch(Exception e){
        // 处理 Class.forName 错误
        e.printStackTrace();
    }finally{
        // 关闭资源
        try{
            if(stmt!=null) stmt.close();
        }catch(SQLException se2){
        }// 什么都不做
        try{
            if(conn!=null) conn.close();
        }catch(SQLException se){
            se.printStackTrace();
        }
    }
    System.out.println("Goodbye!");
	*/
	
	
	
	
	
	
	
}
