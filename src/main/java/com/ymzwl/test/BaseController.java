package com.ymzwl.test;

import java.io.File;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.system.ApplicationHome;
import org.springframework.web.multipart.MultipartFile;





public class BaseController {
	public void outJson(HttpServletResponse response,String json){
		try {
			response.setContentType("application/json; charset=utf-8");  
	        response.setCharacterEncoding("UTF-8");
	        OutputStream out = response.getOutputStream();
	        out.write(json.getBytes("UTF-8"));  
	        out.flush();  
	        out.close();
            Log.outLog(json);
            
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void outString(HttpServletResponse response,String json){
		try {
	        response.setCharacterEncoding("UTF-8");
	        OutputStream out = response.getOutputStream();  
	        out.write(json.getBytes("UTF-8"));  
	        out.flush();  
	        out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 保存文件
	 * @param file 文件
	 * @param fileName 文件名带扩展名
	 */
	public void saveFile(MultipartFile file,String fileName)throws Exception{
		// 详见 App.WebMvcConfigurer 类
		String filePath = new ApplicationHome(this.getClass()).getSource().getParentFile().getPath() + "/upload/";
		File dest = new File(filePath + fileName);
		file.transferTo(dest);
	}

}
