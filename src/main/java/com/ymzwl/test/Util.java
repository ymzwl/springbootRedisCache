package com.ymzwl.test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class Util {
	public static JSONArray list_to_JSONArray(List<Map<String, Object>> list) {
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < list.size(); i++) {
			Map map = list.get(i);
			Iterator iter = map.entrySet().iterator();
			JSONObject jsonObject = new JSONObject();
			while (iter.hasNext()) {
				Map.Entry entry = (Map.Entry) iter.next();
				Object key = entry.getKey();
				Object value = entry.getValue();
				if(value==null){value="";}//解决查询的值为空时候，没有字段名问题，详见spring-mybatis-configuration.xml
				jsonObject.put((String) key, value);
			}
			jsonArray.add(jsonObject);
		}
		return jsonArray;
	}


	public static String uuid() {
		return UUID.randomUUID().toString().replace("-", "");
	}

	/**
	 * json大写转小写
	 * 
	 * @param jSONArray1
	 *            jSONArray1
	 * @return JSONObject
	 */
	public static JSONObject transToLowerObject(String json) {
		JSONObject jSONArray2 = new JSONObject();
		JSONObject jSONArray1 = JSONObject.fromObject(json);
		Iterator it = jSONArray1.keys();
		while (it.hasNext()) {
			String key = (String) it.next();
			Object object = jSONArray1.get(key);
			if (object.getClass().toString().endsWith("JSONObject")) {
				jSONArray2.accumulate(key.toLowerCase(),
						transToLowerObject(object.toString()));
			} else if (object.getClass().toString().endsWith("JSONArray")) {
				jSONArray2.accumulate(key.toLowerCase(),
						transToArray(jSONArray1.getJSONArray(key).toString()));
			} else {
				jSONArray2.accumulate(key.toLowerCase(), object);
			}
		}
		return jSONArray2;
	}

	/**
	 * jsonArray转jsonArray
	 * 
	 * @param jSONArray1
	 *            jSONArray1
	 * @return JSONArray
	 */
	public static JSONArray transToArray(String jsonArray) {
		JSONArray jSONArray2 = new JSONArray();
		JSONArray jSONArray1 = JSONArray.fromObject(jsonArray);
		for (int i = 0; i < jSONArray1.size(); i++) {
			Object jArray = jSONArray1.getJSONObject(i);
			if (jArray.getClass().toString().endsWith("JSONObject")) {
				jSONArray2.add(transToLowerObject(jArray.toString()));
			} else if (jArray.getClass().toString().endsWith("JSONArray")) {
				jSONArray2.add(transToArray(jArray.toString()));
			}
		}
		return jSONArray2;
	}
}
